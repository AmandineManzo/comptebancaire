#ifndef MONEXCEPTION_H_INCLUDED
#define MONEXCEPTION_H_INCLUDED

#include <iostream>

using namespace std;


class MonException : public exception
{
    private:
        string cause;
    public:
        MonException(string c) /*throw()*/ : cause(c) {}
        ~MonException() /*throw() */
        {

            cout << "Destruction Exception !" << endl;
        }

        const char* what() /*const throw()*/
        {
            return cause.c_str();
        }

        string message()
        {
             return cause;
        }
};


#endif // MONEXCEPTION_H_INCLUDED
