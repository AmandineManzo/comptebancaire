#include <iostream>
#include "Client.h"
#include "Particulier.h"
#include "Professionnel.h"
#include "FichierClient.h"
#include "MonException.h"

using namespace std;

int main()
{
    try
    {

    Particulier * p;
    Professionnel * p2;
    p = new Particulier("dupont","paUl",'H',"0442548059","8, Allee de la Pomme","Les oliviers",13000,"mArSeIllE",
                  25,12,11,1995,SituationFamiliale::Celibataire);

    p2 = new Professionnel("DUCOBU","Huguette",'F',"0449048059","35, Rue du jardin","Les aeriennes",31000,"TOULOUSE",
                     "12345678901234","LA BOULANGERIE",2005,"hug.DUC@boulangerie.com", "1, Grand Rue","Le Napoli",31000,"TOULOUSE");

    FichierClient F("Banque1");
    F.ajouter(p);
    F.ajouter(p2);

    p->creationCompte(new Compte(p,234,22,06,2020,1000,250));
    p2->creationCompte(new Compte(p2,567, 23, 07, 2019, 5000, 1000));

    //F.creationCompte(new Compte(p,234,22,06,2020,1000,250));
    //F.creationCompte(new Compte(p2,567, 23, 07, 2019, 5000, 1000));

    cout << "   Bienvenue dans votre Gestion de Compte Client !" <<endl;
    cout << "_____________________________________________________" << endl << endl;

    char choix ='0';

    do
    {

    cout << "Vous souhaitez : " << endl;
    cout << "   1. Lister l'ensemble des clients" << endl;
    cout << "   2. Consulter les soldes des comptes pour un client donn�" << endl;
    cout << "   3. Ajouter/Supprimer/Modifier un Client Particulier ou Professionel" << endl;
    cout << "   4. Ajouter/Supprimer/Modifier une Operation quelque soit le type" <<endl;
    cout << "   5. Afficher l'ensemble des operations pour un compte donne" << endl;
    cout << "   6. Importer le fichier des Operations Bancaires" << endl;

    int switch1 = 0;
    cin >> switch1;

    string nomClient;
    string prenomClient;
    Client * c = nullptr;

    int switch2=0;
    int switch3=0;

    //infos saisie client
    Particulier * a1=nullptr;
    Professionnel * a2 =nullptr;
    char sexe;
    string tel;
    string add_lib;
    string add_comp;
    int add_CP;
    string add_ville;

    //infos saisie particulier
    int situFam;
    int age;
    int jour;
    int mois;
    int annee;

    //infos saisie professionel
    string siret;
    string raisSoc;
    int anneeCreation;
    string mail;

    string addpro_lib;
    string addpro_comp;
    int addpro_CP;
    string addpro_ville;

    int posASuppr=0;

    int switch4=0;

    int numCompteRech=0;
    Compte * compteRech=nullptr;

    string dateOp;
    int codeOp;
    double montOp;
    Operation * op=nullptr;
    int posOp;

    switch(switch1)
    {
    case 1:
        F.afficherTout();
        break;

    case 2:
        cout << "Entrez le nom du client concerne" << endl;
        cin >> nomClient;
        cout << "Entrez le prenom du client concerne" << endl;
        cin >> prenomClient;
        c=F.rechByNom(nomClient,prenomClient);

        if(c!=nullptr)
        {
            cout << c->consultationCompte();
        }
        else cout << "Client inconnu" << endl;
        break;

    case 3:
        cout << "       1. Ajouter un client" << endl;
        cout << "       2. Supprimer un client" << endl;
        cout << "       3. Modifier un client" << endl;
        cin >> switch2;

        switch(switch2)
        {
        case 1:
            cout << "       1. Particulier" << endl;
            cout << "       2. Professionel" << endl;
            cin >> switch3;

            switch(switch3)
            {
            case 1:
                cout << "Veuillez renseigner les informations suivantes : " << endl;
                cout << "Nom : " << endl;
                cin >> nomClient;
                fflush(stdin);
                cout << "Prenom : " << endl;
                cin >> prenomClient;
                fflush(stdin);
                cout << "Sexe (H/F) : " << endl;
                cin >> sexe;
                fflush(stdin);
                cout << "Telephone (10 chiffres) : " << endl;
                cin >> tel;
                fflush(stdin);
                cout << "Adresse (libele) : " << endl;
                getline(cin,add_lib);
                fflush(stdin);
                cout << "Adresse (complement) : " << endl;
                getline(cin,add_comp);
                fflush(stdin);
                cout << "Adresse (Code Postal) : " << endl;
                cin >> add_CP;
                fflush(stdin);
                cout << "Adresse (Ville) : " << endl;
                getline(cin,add_ville);
                fflush(stdin);

                cout << "Situation Familiale (1:Celibataire, 2:Marie, 3:Divorce, 4:Autre) : " << endl;
                cin >> situFam;
                fflush(stdin);
                cout << "Age : " << endl;
                cin >> age;
                fflush(stdin);
                cout << "Date de naissance : " << endl;
                cout << "Jour : " << endl;
                cin >> jour;
                fflush(stdin);
                cout << "Mois : " << endl;
                cin >> mois;
                fflush(stdin);
                cout << "Annee : " << endl;
                cin >> annee;
                fflush(stdin);

                if(situFam==1)
                a1= new Particulier(nomClient,prenomClient,sexe,tel,add_lib,add_comp,add_CP,add_ville,age,jour,mois,annee,SituationFamiliale::Celibataire);
                if(situFam==2)
                a1= new Particulier(nomClient,prenomClient,sexe,tel,add_lib,add_comp,add_CP,add_ville,age,jour,mois,annee,SituationFamiliale::Marie);
                if(situFam==3)
                a1 = new Particulier(nomClient,prenomClient,sexe,tel,add_lib,add_comp,add_CP,add_ville,age,jour,mois,annee,SituationFamiliale::Divorce);
                if(situFam==4)
                a1 = new Particulier(nomClient,prenomClient,sexe,tel,add_lib,add_comp,add_CP,add_ville,age,jour,mois,annee,SituationFamiliale::Autre);

                F.ajouter(a1);
                break;

            case 2:
                cout << "Veuillez renseigner les informations suivantes : " << endl;
                cout << "Nom : " << endl;
                cin >> nomClient;
                fflush(stdin);
                cout << "Prenom : " << endl;
                cin >> prenomClient;
                fflush(stdin);
                cout << "Sexe (H/F) : " << endl;
                cin >> sexe;
                fflush(stdin);
                cout << "Telephone (10 chiffres) : " << endl;
                cin >> tel;
                fflush(stdin);
                cout << "Adresse (libele) : " << endl;
                getline(cin,add_lib);
                fflush(stdin);
                cout << "Adresse (complement) : " << endl;
                getline(cin,add_comp);
                fflush(stdin);
                cout << "Adresse (Code Postal) : " << endl;
                cin >> add_CP;
                fflush(stdin);
                cout << "Adresse (Ville) : " << endl;
                getline(cin,add_ville);
                fflush(stdin);

                cout << "Siret (14 chiffres) :" << endl;
                cin >> siret;
                fflush(stdin);
                cout << "Raison Sociale : " << endl;
                getline(cin,raisSoc);
                fflush(stdin);
                cout << "Annee de creation : " << endl;
                cin >> anneeCreation;
                fflush(stdin);
                cout << "Mail : " << endl;
                cin >> mail;
                fflush(stdin);
                cout << "Adresse pro (libele) : " << endl;
                getline(cin,addpro_lib);
                fflush(stdin);
                cout << "Adresse pro (complement) : " << endl;
                getline(cin,addpro_comp);
                fflush(stdin);
                cout << "Adresse pro (Code Postal) : " << endl;
                cin >> addpro_CP;
                fflush(stdin);
                cout << "Adresse pro (Ville) : " << endl;
                getline(cin,addpro_ville);
                fflush(stdin);

                a2 = new Professionnel(nomClient,prenomClient,sexe,tel,add_lib,add_comp,add_CP,add_ville,siret,raisSoc,anneeCreation,mail,addpro_lib,addpro_comp,addpro_CP,addpro_ville);
                F.ajouter(a2);
                break;
            }
        break;

        case 2:
            cout << "Entrez le nom du client concerne" << endl;
            cin >> nomClient;
            cout << "Entrez le prenom du client concerne" << endl;
            cin >> prenomClient;
            posASuppr=F.rechByNomSuppr(nomClient,prenomClient);

            if(posASuppr!=-1)
            {
                F.supprimer(posASuppr);
                cout << nomClient << " " << prenomClient << " a bien ete supprime !" << endl;
            }
            else cout << "Client inconnu" << endl;
            break;

        case 3:
            cout << "Entrez le nom du client concerne" << endl;
            cin >> nomClient;
            cout << "Entrez le prenom du client concerne" << endl;
            cin >> prenomClient;
            c=F.rechByNom(nomClient,prenomClient);

            cout << "Modification pas encore realisable : supprime et recree :D" << endl;
            break;

        default:
            cout << "Bonne continuation ! " << endl;
            break;
          }
        break;

    case 4:
        cout << "Entrez le nom du client concerne" << endl;
        cin >> nomClient;
        cout << "Entrez le prenom du client concerne" << endl;
        cin >> prenomClient;
        c=F.rechByNom(nomClient,prenomClient);
        for(auto unCompte:c->getComptes())
        {
            cout << endl << unCompte->afficheCompte() << endl;
        }

        cout << "Entrez le numero de compte concerne" << endl;
        cin >> numCompteRech;

        compteRech=c->rechByNumero(numCompteRech);

        if(compteRech!=nullptr)
        {
            cout << "       1. Ajouter une operation" << endl;
            cout << "       2. Supprimer une operation" << endl;
            cout << "       3. Modifier une operation" << endl;
            cin >> switch4;

            switch(switch4)
            {
            case 1:
                cout << "Indiquer la date de l'operation (AAAAMMJJ) :" << endl;
                cin >> dateOp;
                fflush(stdin);
                cout << "Indiquer la nature de l'operation (1. Retrait DAB, 2.Paiement CB, 3.Depot Guichet) : " << endl;
                cin >> codeOp;
                fflush(stdin);
                cout << "Indiquer le montant de l'operation en Euros :" << endl;
                cin >> montOp;
                fflush(stdin);

                op = new Operation(dateOp,codeOp,montOp);
                compteRech->ajouter(op);
                break;

            case 2:
                cout << compteRech->afficherTout();
                cout << "Renseignez l'operation a supprimer : " << endl;
                cin >> posOp;
                compteRech->supprimer(posOp);
                break;
            case 3:
                cout << "Modification non disponible" << endl;
                break;
            }

        }
        else
        {
            cout << "Numero de compte inconnu " << endl;
        }
        break;

    case 5:
        cout << "Entrez le nom du client concerne" << endl;
        cin >> nomClient;
        cout << "Entrez le prenom du client concerne" << endl;
        cin >> prenomClient;
        c=F.rechByNom(nomClient,prenomClient);
        for(auto unCompte:c->getComptes())
        {
            cout << endl << unCompte->afficheCompte() << endl;
        }

        cout << "Entrez le numero de compte concerne" << endl;
        cin >> numCompteRech;

        compteRech=c->rechByNumero(numCompteRech);
        cout << compteRech->afficherTout();
        break;

    case 6:
        cout << "Non implemente" << endl;
        break;

    default:
        cout << "Entr�e invalide, veuillez recommencer" << endl;
        break;

    }

    cout << "Souhaitez vous continuer ? (O/N)" << endl;
    cin >> choix;

    }while(choix=='O');

    }
    catch(MonException &ex)
    {
        cout << "MonException " << ex.message() << endl;
    }
    catch(std::exception& ex)
    {
        cout  << "Erreur (exception): " << ex.what() << endl;
    }
    catch(...)
    {
        cout  << "Autre Erreur" << endl;
    }

    return 0;
}
