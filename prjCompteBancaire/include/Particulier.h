#ifndef PARTICULIER_H
#define PARTICULIER_H

#include "Client.h"


enum class SituationFamiliale {Celibataire, Marie, Divorce, Autre};



class Particulier : public Client
{
    public:
        Particulier(string="\0",string="\0",char='H',string="\0",string="\0",string="\0",int=0,string="\0",int=0,int=0,int=0,int=0,SituationFamiliale=SituationFamiliale::Autre);
        virtual ~Particulier();

        int Getage() { return age; }
        void Setage(int val) {  if(val>0)
                                age = val;
                                else throw MonException("Age invalide"); }

        int Getjour() { return jour; }
        void Setjour(int val) { if(val>0 && val<32)
                                jour = val;
                                else throw MonException("Jour de naissance invalide"); }

        int Getmois() { return mois; }
        void Setmois(int val) { if(val>0 && val<13)
                                mois = val;
                                else throw MonException("Mois de naissance invalide"); }

        int Getannee() { return annee; }
        void Setannee(int val) {  if(val>1919 && val<2021)
                                  annee = val;
                                  else throw MonException("Annee de naissance invalide"); }

        string getSitu();
        void setSitu(SituationFamiliale);

        virtual string affiche() override;
        virtual int getNbComptes() override;
        virtual string consultationCompte() override;
        virtual void creationCompte(Compte*) override;

    protected:

    private:
        SituationFamiliale situFam;
        int age;
        int jour;
        int mois;
        int annee;



};

#endif // PARTICULIER_H
