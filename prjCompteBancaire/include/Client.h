#ifndef CLIENT_H
#define CLIENT_H
#include <iostream>
#include <sstream>
#include <cstring>
#include <string>
#include <vector>
#include "Compte.h"
#include <algorithm>
#include "../MonException.h"

using namespace std;

class Compte;

 string str_toupper(string s);
 string str_toupperFirst(string s);


class Client
{
    public:
        Client(string="\0",string="\0",char='H',string="\0",string="\0",string="\0",int=0,string="\0");
        virtual ~Client();

        string Getnom() { return str_toupper(nom); }
        void Setnom(string val) { nom = val; }

        string Getprenom() { return str_toupperFirst(prenom); }
        void Setprenom(string val) { prenom = val; }

        char Getsexe() { return toupper(sexe); }
        void Setsexe(char val) {
                                if(val=='H' || val=='F' || val=='h' || val=='f')
                                sexe = val;
                                else throw MonException("Sexe incorrect (H/F)"); }

        string Gettel() { return tel; }
        void Settel(string val) { if (strlen(val.c_str())==10)
                                  tel = val;
                                  else throw MonException("Le telephone doit contenir 10 chiffres"); }

        string Getadd_lib() { return add_lib; }
        void Setadd_lib(string val) { add_lib = val; }

        string Getadd_comp() { return add_comp; }
        void Setadd_comp(string val) { add_comp = val; }

        int Getadd_CP() { return add_CP; }
        void Setadd_CP(int val) { add_CP = val; }

        string Getadd_ville() { return str_toupper(add_ville); }
        void Setadd_ville(string val) { add_ville = val; }

        virtual string affiche()=0;
        virtual string consultationCompte()=0;
        virtual int getNbComptes()=0;
        virtual void creationCompte(Compte*)=0;
        vector<Compte*> getComptes();
        Compte * rechByNumero(int);


    protected:
        vector<Compte *> vecComptes;

    private:
        string nom;
        string prenom;
        char sexe;
        string tel;
        string add_lib;
        string add_comp;
        int add_CP;
        string add_ville;


};



#endif // CLIENT_H
