#ifndef PROFESSIONNEL_H
#define PROFESSIONNEL_H

#include "Client.h"


class Professionnel : public Client
{
    public:
        Professionnel(string="\0",string="\0",char='H',string="\0",string="\0",string="\0",int=0,string="\0",
                      string="\0",string="\0",int=0,string="\0", string="\0",string="\0",int=0,string="\0");
        virtual ~Professionnel();

        string Getsiret() { return siret; }
        void Setsiret(string val) { if (strlen(val.c_str())==14)
                                    siret = val;
                                    else throw MonException("Le numero de siret doit contenir 14 chiffres"); }


        string GetraisSoc() { return str_toupper(raisSoc); }
        void SetraisSoc(string val) { raisSoc = val; }

        int GetanneeCreation() { return anneeCreation; }
        void SetanneeCreation(int val) { anneeCreation = val; }

        string Getmail() { return mail; }
        void Setmail(string val) { mail = val; }

        string Getaddpro_lib() { return addpro_lib; }
        void Setaddpro_lib(string val) { addpro_lib = val; }
        string Getaddpro_comp() { return addpro_comp; }
        void Setaddpro_comp(string val) { addpro_comp = val; }

        int Getaddpro_CP() { return addpro_CP; }
        void Setaddpro_CP(int val) { addpro_CP = val; }

        string Getaddpro_ville() { return str_toupper(addpro_ville); }
        void Setaddpro_ville(string val) { addpro_ville = val; }

        virtual string affiche() override;
        virtual void creationCompte(Compte*) override;
        virtual int getNbComptes() override;
        virtual string consultationCompte() override;



    protected:

    private:
        string siret;
        string raisSoc;
        int anneeCreation;
        string mail;

        string addpro_lib;
        string addpro_comp;
        int addpro_CP;
        string addpro_ville;
};

#endif // PROFESSIONNEL_H
