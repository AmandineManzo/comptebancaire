#ifndef COMPTE_H
#define COMPTE_H

#include <iostream>
#include <sstream>
#include <cstring>
#include <string>
#include <vector>
#include <algorithm>

#include "Operation.h"
#include "Client.h"

class Client;

using namespace std;

class Compte
{
    public:
        Compte(Client*,int=0, int=0, int=0, int=0, double=0, double=0);
        virtual ~Compte();

        int Getnumero() { return numero; }
        void Setnumero(int val) { numero = val; }

        int Getjour() { return jour; }
        void Setjour(int val) { if(val>0)
                                jour = val;
                                else throw "Jour de creation de compte invalide"; }

        int Getmois() { return mois; }
        void Setmois(int val) { if(val>0)
                                mois = val;
                                else throw "Mois de creation de compte invalide"; }

        int Getannee() { return annee; }
        void Setannee(int val) {  if(val>1919)
                                  annee = val;
                                  else throw "Annee de creation de compte invalide"; }


        double Getsolde() { return solde; }
        void Setsolde(double val) { solde = val; }

        double Getdecouvert() { return decouvert; }
        void Setdecouvert(double val) { decouvert = val; }

        string afficheCompte();
        void ajouter(Operation *);
        void supprimer(int=0);
        void afficherOpe(int=0);
        string afficherTout();
        int getEffectif();


    protected:

    private:
        int numero;
        int jour;
        int mois;
        int annee;
        double solde;
        double decouvert;
        vector<Operation*> vecOpe;
        Client * c;
};

#endif // COMPTE_H
