#ifndef OPERATION_H
#define OPERATION_H
#include <iostream>
#include <sstream>
#include <cstring>
#include <string>

using namespace std;

class Operation
{
    public:
        Operation(string="00000000",int=0,double=0);
        virtual ~Operation();

        string Getdate() { return date; }
        void Setdate(string val) {  if(strlen(val.c_str())==8)
                                    date = val;
                                    else throw "Date operation invalide"; }

        int Getcode() { return code; }
        void Setcode(int val) { code = val; }

        double Getmontant() { return montant; }
        void Setmontant(double val) {   if(val>0)
                                        montant = val;
                                        else throw "Le montant de l'operation doit etre positif"; }

        string infos();

    protected:

    private:
        string date;
        int code;
        double montant;
};

#endif // OPERATION_H
