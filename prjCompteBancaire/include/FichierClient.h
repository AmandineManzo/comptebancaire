#ifndef FICHIERCLIENT_H
#define FICHIERCLIENT_H

#include <iostream>
#include <vector>
#include <algorithm>
#include "Client.h"
#include "Compte.h"

using namespace std;

class FichierClient
{
    public:
        FichierClient(string);
        virtual ~FichierClient();

        string Getnom() { return nom; }
        void Setnom(string val) { nom = val; }

        void ajouter(Client *);
        void supprimer(int);
        void afficherClient(int=0);
        void afficherTout();
        int getEffectif();
        void tri();
        Client* rechByNom(string,string);
        int rechByNomSuppr(string,string);
        //Compte * rechByNumero(int=0);
        //void creationCompte(Compte*);

    protected:

    private:
        vector<Client*> vecClients;
        //vector<Compte *> vecComptes;
        string nom;
};






#endif // FICHIERCLIENT_H
