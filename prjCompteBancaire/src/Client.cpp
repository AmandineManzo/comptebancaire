#include "Client.h"

Client::Client(string n, string p, char S, string T, string L, string C, int cp, string V)
{
    this->Setnom(n);
    this->Setprenom(p);
    this->Setsexe(S);
    this->Settel(T);
    this->Setadd_lib(L);
    this->Setadd_comp(C);
    this->Setadd_CP(cp);
    this->Setadd_ville(V);


}

vector<Compte*> Client::getComptes()
{
    return vecComptes;
}

Compte * Client::rechByNumero(int num)
{
    for(int i=0; i<vecComptes.size();i++ )
    {
        if (vecComptes[i]->Getnumero()==num) return vecComptes[i];
    }
    return nullptr;
}

Client::~Client()
{
    cout << "Destruction du client " << Getnom() << endl;
}


 string str_toupper(string s) {
    transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return toupper(c); } // correct
                  );
    return s;
}


string str_toupperFirst(string s)
{
    transform(s.begin(), s.begin()+1, s.begin(),
                   [](unsigned char c){ return toupper(c);} );
    transform(s.begin()+1, s.end(), s.begin()+1,
                   [](unsigned char c){ return tolower(c);} // correct
             );
    return s;
}
