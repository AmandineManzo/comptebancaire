#include "Compte.h"

Compte::Compte(Client* p,int num, int j, int m, int a, double s, double d)
{
    this->Setnumero(num);
    this->Setjour(j);
    this->Setmois(m);
    this->Setannee(a);
    this->Setsolde(s);
    this->Setdecouvert(d);
}

string Compte::afficheCompte()
{
    stringstream oss;

    oss << "Numero de compte : " << Getnumero() << endl;
    oss << "Creation du compte : " << Getjour() << "/" << Getmois() << "/" << Getannee() << endl;
    oss << "Decouvert autorise : " << Getdecouvert() << " Euros" << endl;
    oss << "Solde : " << Getsolde() << " Euros" << endl;

    return oss.str();
}


void Compte::ajouter(Operation* ope)
{
    vecOpe.push_back(ope);

    if (ope->Getcode()==1 || ope->Getcode()==2) Setsolde(this->Getsolde()-ope->Getmontant());
    if (ope->Getcode()==3) Setsolde(this->Getsolde()+ope->Getmontant());

}

void Compte::supprimer(int posASupprimer)
{
    if (posASupprimer < vecOpe.size())
    {
        delete vecOpe[posASupprimer];
        vecOpe.erase(vecOpe.begin() + posASupprimer);
    }
    else
        cout << "Aucun element ne correspond a cette position : " << posASupprimer << endl;
}

void Compte::afficherOpe(int posARechercher)
{
    try
    {
        auto p= vecOpe.at(posARechercher);

        cout << p->infos();

    }
    catch(exception& ex)
    {
        cout << "Position Introuvable " << ex.what();
    }
}




string Compte::afficherTout()
{
    stringstream oss;

    oss << endl << "Nombre d'Operations : " << this->getEffectif() << endl;
    for(auto elm : vecOpe)
    {
        oss << "Nom : " << typeid(elm).name() << endl;
        oss << elm->infos();
        oss << endl << "---------------------------------------" << endl;
    }

    return oss.str();
}

int Compte::getEffectif()
{
    return count_if(vecOpe.begin(), vecOpe.end(), [](auto p){return true;});
}


Compte::~Compte()
{
    for(int i=0; i < vecOpe.size() ; i++)
    {
        delete vecOpe[i];
    }
    cout << "Destruction du compte numero : " << Getnumero() << endl;
}
