#include "Professionnel.h"

Professionnel::Professionnel(string n, string p, char S, string T, string L, string C, int cp, string V,
                             string sir,string rais,int ann,string ma, string lp,string compp,int cpp,string vp)
                             :Client(n,p,S,T,L,C,cp,V)
{
    this->Setsiret(sir);
    this->SetraisSoc(rais);
    this->SetanneeCreation(ann);
    this->Setmail(ma);
    this->Setaddpro_lib(lp);
    this->Setaddpro_comp(compp);
    this->Setaddpro_CP(cpp);
    this->Setaddpro_ville(vp);

}


string Professionnel::affiche()
{
    stringstream oss;

    oss << "Siret : " << Getsiret() << endl;
    oss << GetraisSoc() << endl;
    oss << "Anciennete : " << 2020-GetanneeCreation() << endl;
    oss << Getaddpro_lib() << endl;
    oss << Getaddpro_comp() << endl;
    oss << Getaddpro_CP() << " " << Getaddpro_ville() << endl;

    if (Getsexe()=='H') oss << endl << "M. " << (Getnom()) << " " << Getprenom() << endl;

    if (Getsexe()=='F') oss << endl << "Mme. " << (Getnom()) << " " << Getprenom() << endl;
    oss << "Telephone : " << Gettel() << endl;
    oss << "Mail : " << Getmail() << endl;


    return oss.str();
}

int Professionnel::getNbComptes()
{
    return count_if(vecComptes.begin(), vecComptes.end(), [](auto p){return true;});
}

void Professionnel::creationCompte(Compte * cpt)
{
    vecComptes.push_back(cpt);
}



string Professionnel::consultationCompte()
{
    stringstream oss;

    oss << endl << "Nombre de comptes : " << this->getNbComptes() << endl;
    oss << "________________________" << endl << endl;
    for(auto elm : vecComptes)
    {
        oss << elm->afficheCompte();
        oss << elm->afficherTout();
        oss << "---------------------------------------" << endl << endl;
    }

    return oss.str();
}


Professionnel::~Professionnel()
{
    cout << "Destruction Professionel : " << Getnom() << endl;
}
