#include "Operation.h"

Operation::Operation(string d, int c, double m)
{
    this->Setdate(d);
    this->Setcode(c);
    this->Setmontant(m);
}

string Operation::infos()
{
    stringstream oss;

    if(Getcode()==1)
    {
        oss << "Retrait de " << Getmontant() << " Euros, du " << Getdate() << endl;
    }

    if(Getcode()==2)
    {
        oss << "Carte Bleue de " << Getmontant() << " Euros, du " << Getdate() << endl;
    }

    if(Getcode()==3)
    {
        oss << "Depot de " << Getmontant() << " Euros, du " << Getdate() << endl;
    }

    return oss.str();
}



Operation::~Operation()
{
    cout << "Destruction de l'operation du : " << Getdate() << endl;
}
