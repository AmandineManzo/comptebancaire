#include "FichierClient.h"

FichierClient::FichierClient(string n)
{
    this->nom = n;
}

void FichierClient::ajouter(Client* c)
{
    vecClients.push_back(c);
}

void FichierClient::supprimer(int posASupprimer)
{
    if (posASupprimer < vecClients.size())
    {
        vecClients.erase(vecClients.begin() + posASupprimer);
    }
    else
        cout << "Aucun client ne correspond a cette position : " << posASupprimer << endl;
}



void FichierClient::afficherClient(int posARechercher)
{
    try
    {
        auto p= vecClients.at(posARechercher);


        p->affiche();

    }
    catch(exception& ex)
    {
        cout << "Position Client Introuvable " << ex.what();
    }
}

void FichierClient::afficherTout()
{
    cout << endl << "Nombre de clients : " << this->getEffectif() << " clients" << endl;
    for(auto elm : vecClients)
    {
        //cout << "Nom : " << typeid(elm).name() << endl;
        cout << elm->affiche();
        cout << endl << "---------------------------------------" << endl;
    }
}

int FichierClient::getEffectif()
{
    return count_if(vecClients.begin(), vecClients.end(), [](auto p){return true;});
}

void FichierClient::tri()
{

}

Client * FichierClient::rechByNom(string nomARech, string prenomARech)
{
    for(auto c:vecClients)
        //for(int i=0; i < vecClients.size() ; i++)
    {
        if (c->Getnom().compare(nomARech)==0 && c->Getprenom().compare(prenomARech)==0) return c;
    }
    return nullptr;
}

int FichierClient::rechByNomSuppr(string nomARech, string prenomARech)
{
    for(int i=0; i<vecClients.size();i++ )
    {
        if (vecClients[i]->Getnom().compare(nomARech)==0 && vecClients[i]->Getprenom().compare(prenomARech)==0) return i;
    }
    return -1;
}

/*Compte * FichierClient::rechByNumero(int num)
{
    for(int i=0; i<vecComptes.size();i++ )
    {
        if (vecComptes[i]->Getnumero()==num) return vecComptes[i];
    }
    return nullptr;
}
*/
/*void FichierClient::creationCompte(Compte * cpt)
{
    vecComptes.push_back(cpt);
}*/


FichierClient::~FichierClient()
{
    for(int i=0; i < vecClients.size() ; i++)
    {
        delete vecClients[i];
    }
    cout << "Destruction Fichier client " << this->nom << endl;
}
