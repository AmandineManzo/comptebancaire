#include "Particulier.h"


Particulier::Particulier(string n, string p, char S, string T, string L, string C, int cp, string V,int a, int j, int m, int an, SituationFamiliale s)
    :Client(n,p,S,T,L,C,cp,V)
{
    this->Setage(a);
    this->Setjour(j);
    this->Setmois(m);
    this->Setannee(an);
    this->setSitu(s);
}

string Particulier::affiche()
{
    stringstream oss;

    if (Getsexe()=='H') oss << "M. " << (Getnom()) << " " << Getprenom() << endl;

    if (Getsexe()=='F') oss << "Mme. " << (Getnom()) << " " << Getprenom() << endl;

    oss << Getadd_lib() << endl;
    oss << Getadd_comp() << endl;
    oss << Getadd_CP() << " " << Getadd_ville() << endl;

    oss << endl << "Telephone : " << Gettel() << endl;

    oss << "Situation Familiale : " << getSitu() << endl;
    oss << "Age : " << Getage() << " ans" << endl;



    return oss.str();

}

void Particulier::creationCompte(Compte * cpt)
{
    vecComptes.push_back(cpt);
}



int Particulier::getNbComptes()
{
    return count_if(vecComptes.begin(), vecComptes.end(), [](auto p){return true;});
}

string Particulier::consultationCompte()
{
    stringstream oss;

    oss << endl << "Nombre de comptes : " << this->getNbComptes() << endl;
    oss << "________________________" << endl << endl;
    for(auto elm : vecComptes)
    {
        oss << elm->afficheCompte();
        oss << elm->afficherTout();
        oss << "---------------------------------------" << endl << endl;
    }

    return oss.str();
}




string Particulier::getSitu()
{
    string libelle="\0";
    switch(situFam)
    {
        case SituationFamiliale::Celibataire:
            libelle= "Celibataire";
            break;
        case SituationFamiliale::Marie:
            libelle= "Marie(e)";
            break;
        case SituationFamiliale::Divorce:
            libelle= "Divorce(e)";
            break;
        default:
            libelle= "Autre";
            break;
    }
    return libelle;
}

void Particulier::setSitu(SituationFamiliale situ)
{
    situFam = situ;
}


Particulier::~Particulier()
{
    cout << "Destruction du particulier " << Getnom() << endl;
}
